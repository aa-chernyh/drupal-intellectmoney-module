#Модуль оплаты платежной системы IntellectMoney для CMS Drupal

> **Внимание!** <br>
Данная версия актуальна на *29 мая 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/Drupal#5575114e981b32f4d145aebd81cf80212f2601.

Страница модуля на marketplace: https://www.drupal.org/sandbox/maxcpr/2694905
<br>
Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/Drupal#557511d3e0943833384887ba3dc9e4f599008f
